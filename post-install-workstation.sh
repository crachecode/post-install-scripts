#!/bin/bash

apt-get install -y curl zip rsync htop btop ncdu fzf psmisc pv samba nautilus-share trash-cli
apt-get install -y msmtp msmtp-mta
apt-get install -y git-all

# restic backups
apt-get install -y rclone restic
rclone selfupdate
restic self-update
curl -LO https://raw.githubusercontent.com/creativeprojects/resticprofile/master/install.sh
chmod +x install.sh
./install.sh -b /usr/local/bin
rm install.sh

git clone https://github.com/vinceliuice/vimix-gtk-themes.git
git clone https://github.com/vinceliuice/vimix-icon-theme.git
git clone https://github.com/vinceliuice/Vimix-kde.git
./vimix-gtk-themes/install.sh -l -t all -s all -d /usr/share/themes
./vimix-icon-theme/install.sh -a -d /usr/share/icons
./Vimix-kde/install.sh -d /usr/share/plasma/desktoptheme/

# change nautilus icon
#cp /usr/share/icons/Vimix-ruby-dark/scalable/places/file-manager.svg /usr/share/icons/Vimix-ruby-dark/scalable/apps/system-file-manager.svg

apt-get install -y breeze-cursor-theme bibata-cursor-theme

apt-get install -y flatpak gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#find /usr/share/icons/ -maxdepth 1 -type d | sed 's/\/usr\/share\/icons\///g' | tail -n +2
#find /usr/share/themes/ -maxdepth 1 -type d | sed 's/\/usr\/share\/themes\///g' | tail -n +2
# theme=$(gsettings get org.gnome.desktop.interface gtk-theme)

# choose your prefered GTK theme here : https://github.com/vinceliuice/vimix-gtk-themes
gtk_theme=vimix-dark-ruby

# choose your prefered icon theme here : https://github.com/vinceliuice/vimix-icon-theme
icon_theme=Vimix-ruby-dark

gsettings set org.gnome.desktop.interface gtk-theme "${gtk_theme}"
gsettings set org.gnome.desktop.wm.preferences theme "${gtk_theme}"
gsettings set org.gnome.desktop.interface icon-theme "${icon_theme}"

# copy themes in user directory for use with flatpak
mkdir /home//.themes $HOME/.icons
cp -R /usr/share/themes/vimix* $HOME/.themes
cp -R /usr/share/icons/Vimix* $HOME/.icons
chown -R $USER:$USER $HOME/.themes $HOME/.icons

flatpak override --filesystem=$HOME/.themes
flatpak override --filesystem=$HOME/.icons
flatpak override --env=GTK_THEME=$gtk_theme
flatpak override --env=ICON_THEME=$icon_theme

# get and export constant GTK_THEME as set in gsettings in ~/.profile
echo "gtk_theme=\$(gsettings get org.gnome.desktop.interface gtk-theme)
gtk_theme=\${gtk_theme/#\\'/}; gtk_theme=\${gtk_theme/%\\'/} # remove '
export GTK_THEME=\$gtk_theme" >> ~/.profile

# use GTK theme with Qt apps
apt-get install -y qt5ct

apt-get install -y pipx
pipx ensurepath
source ~/.bashrc
pipx install gnome-extensions-cli --system-site-packages
$HOME/.local/bin/gnome-extensions-cli install gsconnect@andyholmes.github.io user-theme@gnome-shell-extensions.gcampax.github.com material-shell@papyelgringo display-ddc-brightness-volume@sagrland.de
# uncomment next line if you're installing on a laptop :
#gnome-extensions-cli install Battery-Health-Charging@maniacx.github.com

# allow user to share files using samba
usermod -a -G sambashare $USER

# uncomment next line to disable gnome-keyring and use keepass instead :
#chmod -x /usr/bin/gnome-keyring-daemon

apt-get install -y vim solaar geary vlc keepassxc
flatpak install -y io.freetubeapp.FreeTube im.riot.Riot com.rustdesk.RustDesk com.google.EarthPro com.github.PintaProject.Pinta com.github.vikdevelop.photopea_app
flatpak install -y com.github.iwalton3.jellyfin-media-player

# for developers :
apt-get install -y gitlab-cli gh tmux #tmux-plugin-manager tmuxp tmux-themepack-jimeh 
