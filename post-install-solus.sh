#!/bin/bash

eopkg it -y curl zip rsync htop btop ncdu fzf psmisc pv samba nautilus-share openssh-server git

# restic backups
eopkg it -y rclone restic
rclone selfupdate
restic self-update
curl -LO https://raw.githubusercontent.com/creativeprojects/resticprofile/master/install.sh
chmod +x install.sh
./install.sh -b /usr/bin
rm install.sh

# vimix
eopkg it -y sassc
git clone https://github.com/vinceliuice/vimix-gtk-themes.git
git clone https://github.com/vinceliuice/vimix-icon-theme.git
./vimix-gtk-themes/install.sh -l -t all -s all -d /usr/share/themes
./vimix-icon-theme/install.sh -a -d /usr/share/icons

# choose your prefered GTK theme here : https://github.com/vinceliuice/vimix-gtk-themes
gtk_theme=vimix-dark-ruby

# choose your prefered icon theme here : https://github.com/vinceliuice/vimix-icon-theme
icon_theme=Vimix-ruby-dark

gsettings set org.gnome.desktop.interface gtk-theme "${gtk_theme}"
gsettings set org.gnome.desktop.wm.preferences theme "${gtk_theme}"
gsettings set org.gnome.desktop.interface icon-theme "${icon_theme}"

# copy themes in user directory for use with flatpak
mkdir $HOME/.themes $HOME/.icons
cp -R /usr/share/themes/vimix* $HOME/.themes
cp -R /usr/share/icons/Vimix* $HOME/.icons
chown -R $USER:$USER $HOME/.themes $HOME/.icons

flatpak override --filesystem=$HOME/.themes
flatpak override --filesystem=$HOME/.icons
flatpak override --env=GTK_THEME=$gtk_theme
flatpak override --env=ICON_THEME=$icon_theme

# get and export constant GTK_THEME as set in gsettings in ~/.profile
echo "gtk_theme=\$(gsettings get org.gnome.desktop.interface gtk-theme)
gtk_theme=\${gtk_theme/#\\'/}; gtk_theme=\${gtk_theme/%\\'/} # remove '
export GTK_THEME=\$gtk_theme" >> ~/.profile

# use GTK theme with Qt apps
eopkg it -y qt5ct

eopkg it -y pipx
pipx ensurepath
source ~/.bashrc
pipx install gnome-extensions-cli --system-site-packages
$HOME/.local/bin/gnome-extensions-cli install gsconnect@andyholmes.github.io user-theme@gnome-shell-extensions.gcampax.github.com dash-to-panel@jderose9.github.com
# uncomment next line if you're installing on a laptop :
#gnome-extensions-cli install Battery-Health-Charging@maniacx.github.com

# uncomment next line to disable gnome-keyring and use keepass instead :
#chmod -x /usr/bin/gnome-keyring-daemon

eopkg it -y gnome-software vim solaar geary vlc keepassxc
flatpak install -y io.freetubeapp.FreeTube im.riot.Riot com.rustdesk.RustDesk com.google.EarthPro com.github.PintaProject.Pinta com.github.rafostar.Clapper
flatpak install -y com.github.iwalton3.jellyfin-media-player
