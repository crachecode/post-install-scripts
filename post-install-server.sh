#!/bin/bash

apt-get update

# docker
usermod -aG docker $USER
apt-get install -y ca-certificates curl gnupg
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg
# Add the repository to Apt sources:
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# system
apt-get install -y psmisc lshw ethtool

# security
apt-get install -y gnupg

# web
apt-get install -y curl

# mail
apt-get install -y msmtp msmtp-mta

# monitoring
apt-get install -y htop btop iotop pv iperf3

# files
apt-get install -y git-all zip ncdu rsync rclone samba nautilus-share parted rename fzf

# restic backups
apt-get install -y rclone restic
rclone selfupdate
restic self-update
curl -LO https://raw.githubusercontent.com/creativeprojects/resticprofile/master/install.sh
chmod +x install.sh
./install.sh -b /usr/local/bin
rm install.sh

# development
apt-get install -y vim gitlab-cli gh tmux #tmux-plugin-manager tmuxp tmux-themepack-jimeh
