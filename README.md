# post-install-scripts

personal post-install scripts for different debian-based linux environments

- [server](/post-install-server.sh)
- [workstation](/post-install-workstation.sh) for web development and system administration
- standard [desktop](/post-install-desktop.sh) use

## usage

```
git clone https://gitlab.com/crachecode/post-install-scripts.git
cd post-install-scripts
```
- read code to verify config

```
sudo USER=$USER -E bash post-install-xxx.sh
```
